/*  ============================================================================

     MrDIY.ca ULP Trigger Sensors (Hub-less)

     THE SENDER

                       ESP12
                      --------------
                     | RST       TX |
         volt div----| ADC       RX |
                     | CH        05 |
                     | 16        04 |--done-->
                     | 14        00 |
                     | 12        02 |
                     | 13        15 |
                     | V        GND |
                      --------------

   ============================================================================= */


#include <ESP8266WiFi.h>

#define       donePin   4
#define       ssid                  ""
#define       password              ""
WiFiClient    wifiClient;


void setup() {

  digitalWrite(donePin, HIGH);
  pinMode(donePin, OUTPUT);
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) delay(50);

  /* -------------- ADD YOUR CODE HERE ---------------- */

  digitalWrite(donePin, LOW);     //  DEEP SLEEP

}

void loop() {}
